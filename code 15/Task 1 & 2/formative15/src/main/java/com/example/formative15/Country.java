package com.example.formative15;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Date;
import java.util.List;

@Entity
public class Country {
    String code;
    String name;
    Date deleteDate;
    @Id
    int id;
    @OneToMany(targetEntity = Province.class)
    List<Province> provinces;

    public Country(){

    }

    public Country(String code, String name, Date deleteDate, int id, List<Province>provinces){
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.id=id;
        this.provinces=provinces;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public int getId() {
        return id;
    }

    public List<Province> getProvinces(){
        return provinces;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }
}
