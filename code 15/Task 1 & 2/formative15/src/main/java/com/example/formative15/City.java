package com.example.formative15;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Date;
import java.util.List;

@Entity
public class City {
    String code;
    String name;
    Date deleteDate;
    @Id
    int id;
    int countryId;
    int provinceId;
    @OneToMany(targetEntity = District.class)
    List<District> districts;

    public City(){

    }

    public City(String code, String name, Date deleteDate, int id, int countryId, int provinceId,
                List<District>districts){
        this.code = code;
        this.name = name;
        this.deleteDate = deleteDate;
        this.id = id;
        this.countryId = countryId;
        this.provinceId = countryId;
        this.districts = districts;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistrictList(List<District> districtList) {
        this.districts = districts;
    }

}
