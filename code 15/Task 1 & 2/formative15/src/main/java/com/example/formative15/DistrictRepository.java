package com.example.formative15;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DistrictRepository extends CrudRepository<District, Integer> {
    District findById(int id);
    List<District> findAll();
    void deleteById(int id);
    District save(District district);
}
