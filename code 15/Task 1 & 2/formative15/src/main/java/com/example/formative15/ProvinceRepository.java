package com.example.formative15;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProvinceRepository extends CrudRepository<Province, Integer> {
    Province findById(int id);
    List<Province> findAll();
    void deleteById(int id);
    Province save(Province province);
}
