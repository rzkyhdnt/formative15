package com.example.formative15;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @PostMapping(value = "/countries", consumes = "application/json")
    public String addAll(@RequestBody Country country){
        for(Province province : country.getProvinces()){
            for(City city : province.getCity()){
                for(District dist : city.getDistricts()){
                    districtRepository.save(dist);
                }
                cityRepository.save(city);
            }
            provinceRepository.save(province);
        }
        countryRepository.save(country);
        return "Completed";
    }

    @GetMapping("/countries/6")
    public Country allCountries(){
        return countryRepository.findById(6);
    }

    @GetMapping("/")
    public String welcome(){
        return "<html><body>"
                + "<h1> test </h1>"
                + "</body></html>";
    }
}
